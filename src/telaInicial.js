import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import LayoutHorizontal from "./layout/layoutHorizontal";

function TelaInicial({navigation}) {
    return(
        <View style={styles.container}>
          <Text>Tela Inicial</Text>
          <Button title="Ir para segunda página" onPress={()=> navigation.navigate('SegundaTela')}></Button>
          <LayoutHorizontal/>
        </View>
    )
  }

  export default TelaInicial;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
  });
  