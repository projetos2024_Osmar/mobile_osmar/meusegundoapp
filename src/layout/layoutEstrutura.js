import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

export default function Layout() {
  return (
    <View style={styles.container}>
      
      <View style={styles.header}>
        <Text style={styles.text}>MENU</Text>
      </View>

      <View style={styles.content}>
        <ScrollView>
          <Text style={styles.text}>CONTEUDO</Text>
        </ScrollView>
      </View>

      <View style={styles.footer}>
        <Text style={styles.text}>RODAPE</Text>
      </View>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    
  },
  header: {
    backgroundColor: 'blue',
    height: 50,
    
  },
  content: {
    backgroundColor: 'green',
    flex:1,
  },
  footer: {
    backgroundColor: 'red',
    height: 50,
    
  },
  text: {
    fontSize: 20,
    color: '#fff',
    textShadowColor: 'rgba(250, 250, 250, 0.75)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 10,
    textAlign: 'center',
  },
});
