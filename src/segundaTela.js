import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Button } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

function SegundaTela({navigation}) {
    return(
        <View style={styles.container }>
          <Text>Segunda Tela</Text>
          <Button title="Voltar" onPress={()=> navigation.goBack()}></Button>
        </View>
    )
  }

export default SegundaTela;

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
  });
  