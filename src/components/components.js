//import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, TextInput, Button } from 'react-native';
import { useState, useEffect } from 'react';

export default function Components() {
    const [text, setText] = useState("");
    const [numLetras, setNumLetras] = useState(0);

    function click(){
        setNumLetras(text.length)
    };
  return (
    <View style={styles.container}>
        <Text style={styles.text}> Your message: {text}</Text>
        <TextInput style={styles.input} value={text} onChangeText={(textInput) => setText(textInput)} placeholder='Put your text here...'/>
        <Button title='Submit' onPress={()=>{click();}}/>
        {numLetras > 0 ? <Text>{numLetras}</Text> : <Text></Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 28,
    fontWeight: 'bold'
  },
  input: {
    fontSize: 20,
    borderWidth: 2,
    marginVertical: 10,
    width: 250,
    padding: 5,
    borderRadius: 5
  },
  
});
